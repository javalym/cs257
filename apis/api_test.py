import sys
import argparse
import json
import urllib.request

#base_url = 'http://developer.ultralingua.com/api/stems/{0}/{1}'
#url = base_url.format(language, word)

def get_cities(state):
	base_url = 'http://api.wunderground.com/api/6dc0b35f17eceb8f/geolookup/q/{0}.json'
	url = base_url.format(state)
	data_from_server = urllib.request.urlopen(url).read()
	string_from_server = data_from_server.decode('utf-8')
	parsed_json = json.loads(string_from_server)
	if 'error' in parsed_json['response']:
		print("invalid state. Usage: 'ST' where ST is the state abbreviation in caps")
		sys.exit(0)
	return parsed_json

def get_conditions(city, state):

	base_url = 'http://api.wunderground.com/api/6dc0b35f17eceb8f/conditions/q/{0}/{1}.json'
	url = base_url.format(state, city)
	#print(url)
	data_from_server = urllib.request.urlopen(url).read()
	string_from_server = data_from_server.decode('utf-8')
	parsed_json = json.loads(string_from_server)
	if 'error' in parsed_json['response']:
		print("invalid city/state. Usage: '--city city_name' 'ST' where ST is the state abbreviation in caps and the city name is one word with an underscore to separate words.")
		sys.exit(0)
	return parsed_json

def main(args):
	if args.city == None:
		possible_cities = get_cities(args.state)
		print('Here are some of the possible cities you can query in {0}:'.format(args.state))
		cityList = possible_cities['response']['results']
		#print(cityList)
		for item in cityList:
			print(item['city'])
	else:
		conditions = get_conditions(args.city, args.state)
		print('current conditions in {0}, {1}'.format(args.city, args.state))
		weather = conditions['current_observation']['weather']
		temperature = conditions['current_observation']['temp_f']
		windspeed = conditions['current_observation']['wind_mph']
		winddir = conditions['current_observation']['wind_dir']
		feelslike = conditions['current_observation']['feelslike_f']
		print('Weather: {0}\n Temperature (F): {1}\n Wind Speed (MPH): {2}\n Wind Direction: {3}\n Feels like (F): {4}'.format(weather, temperature, windspeed, winddir, feelslike))

if __name__ == '__main__':
    # When I use argparse to parse my command line, I usually
    # put the argparse setup here in the global code, and then
    # call a function called main to do the actual work of
    # the program.
    print("Usage: To list all of the cities in a state that you can query use the argument: 'ST' where ST is the state abbreviation")
    print("To query the current weather for a city use the arguments: '--city city_name' 'ST' where ST is the state abbreviation in caps and the city name is one word with an underscore to separate words.\n")
    print("Note: Not all cities that you can query are listed when providing the ST arguments. For example Northfield is not listed, but can still be used as the city argument.\n")
    parser = argparse.ArgumentParser(description='Get state weather data from the wunderground API')

    parser.add_argument('state',
    					metavar='state',
                        help='the state as a 2-character abbreviation in CAPS')
    parser.add_argument('--city', 
    					metavar='city',
						help='the city whose weather data you would like to look up')
    


    args = parser.parse_args()
    
    main(args)




